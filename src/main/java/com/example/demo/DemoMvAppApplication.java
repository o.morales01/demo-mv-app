package com.example.demo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

@SpringBootApplication
public class DemoMvAppApplication {



	public static void main(String[] args) {
		SpringApplication.run(DemoMvAppApplication.class, args);
		try{
			ArrayList<UF> UFlista = ExtraerUF();
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public static ArrayList<UF> ExtraerUF() throws IOException {
		ArrayList<UF> listaUF = new ArrayList<UF>();
		URL url = new URL("https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=");
		Document doc = Jsoup.connect(url.toString()).get();
		Elements table = doc.select("tr").not(".GridHeader");
		for(Element row : table.subList(1, table.size())){
			String dia = row.select("td").first().text();
			int mes = 0;
			Elements precios = row.getElementsByClass("obs");
			for(Element precio : precios){
				UF uf = new UF();
				mes++;
				if(!precio.text().isEmpty()){
					uf.setPrecio(precio.text());
					uf.setFecha(dia+"/"+mes+"/2021");
					System.out.println(precio.text()+"\t"+dia+"/"+mes+"/2021");
					System.out.println();
					listaUF.add(uf);
				}
			}
		}
		return listaUF;
	}



}
