package com.example.demo;

public class UF {
    private String precio;
    private String fecha;

    public UF() {
        precio = "";
        fecha = "";
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
