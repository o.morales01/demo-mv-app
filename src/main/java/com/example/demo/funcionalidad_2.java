package com.example.demo;

public class funcionalidad_2 {

    private String valor1;
    private String valor2;

    public funcionalidad_2() {
    }

    public funcionalidad_2(String valor1, String valor2) {
        this.valor1 = valor1;
        this.valor2 = valor2;
    }

    public void obtenerVariacionPorcentual(){

        try{
            float numero1 = Integer.parseInt(this.valor1);
            float numero2 = Integer.parseInt(this.valor2);

            double perIncre =((numero2 - numero1)/numero1)*100;
            if(perIncre>0)
                System.out.println("Porcentaje aumentado en "+perIncre+" %");
            else
                System.out.println("Porcentaje disminuido en "+perIncre+" %");

        }catch (NumberFormatException e){
            System.out.println(e);
        }


    }
}


